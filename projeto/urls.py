"""projeto URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from blog import views as blog_views #importa o arquivo views e suas classes na pasta blog com o apelido blog_views
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path('', blog_views.PostListView.as_view()),
    path('comentario/<int:post>', blog_views.CommentCreateView.as_view(), name='comment'),
    path('pesquisa/', blog_views.SearchView.as_view(), name='search'),
    path('categoria/<int:pk>', blog_views.CategoryDetailView.as_view(), name='category-detail'),
    path('post/<int:pk>/', blog_views.PostDetailView.as_view(), name='post-detail'), #para ver um objeto de cada vez
    path('admin/', admin.site.urls),
    path('usuarios/', include('django.contrib.auth.urls')),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)